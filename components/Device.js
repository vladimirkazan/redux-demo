import React, { Component, PropTypes } from 'react'

class Device extends Component {
  render() {
  	const { item } = this.props
  	return (
  	  <li>{item.name}</li>	
  	);
  }
}

Device.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string.isRequired
  }).isRequired
}



export default Device