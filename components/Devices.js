import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

import Device from './Device'

class Devices extends Component {



  render() {
  	const { devices, getAllDevices } = this.props

    return (
      <div>
        <h1>IoT devices</h1>
        <button onClick={() => { 
          getAllDevices() }}>Load Devices</button>
        <ul>
          {devices.map(item =>
            <Device  key={item.id} item={item} />
          )}
        </ul>
      </div>
    )
  }
}

Devices.propTypes = {
  getAllDevices: PropTypes.func.isRequired,
  devices: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired
  })).isRequired,
  isFetching: PropTypes.bool.isRequired,
  loadingLabel: PropTypes.string.isRequired
}


Devices.defaultProps = {
  isFetching: true,
  loadingLabel: 'Loading...'
}

export default Devices


