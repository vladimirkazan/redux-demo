/**
 * Mocking client-server processing
 */
import _data from './devices.json'

const TIMEOUT = 1000

export default {
  getAll(cb, timeout) {
  	//console.log('devices-api: getAll()', _data);
    setTimeout(() => cb(_data), timeout || TIMEOUT)
  }
}
