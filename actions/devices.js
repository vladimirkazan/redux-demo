import devicesApi from '../api/devices-api'

export const LOAD = 'LOAD'

export function load() {
  return {
    type: LOAD,
    devices
  }
}

export function loadAsync(delay = 1000) {
  return dispatch => {
    setTimeout(() => {
      console.log('loaded!');
      dispatch(load())
    }, delay)
  }
}

function receiveDevices(devices) {
  return {
    type: LOAD,
    devices: devices
  }
}

export function getAllDevices() {
  return dispatch => {
    devicesApi.getAll(devices => {
      console.log('getAllDevices', devices);
      dispatch(receiveDevices(devices))
    })
  }
}