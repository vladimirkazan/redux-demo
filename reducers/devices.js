import { LOAD } from '../actions/devices'

export default function counter(state = [], action) {
  console.log('reducers/devices: state', state, 'action', action);
  switch (action.type) {
    case LOAD:
      state = action.devices
      return state
    default:
      return state
  }
}
