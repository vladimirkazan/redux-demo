import 'babel-core/polyfill'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './containers/App'
import configureStore from './store/configureStore'

import { getAll } from './api/devices-api'

const initialState = window.__INITIAL_STATE__
const store = configureStore(initialState)
const rootElement = document.getElementById('app')

//store.dispatch(getAll())

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
