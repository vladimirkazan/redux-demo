Exercise

- Fetch a list of active devices from IoT server.
- Display the list as a selectable set of radio buttons.
- Selecting one of the devices should fetch full current status of the device. Display the attributes of the so-fetched status in a table.
    - The full current status of a device can be fetched by the API: `IoT.Device.get`
- Alongside the name of a device in radioset, display the current status of the device, coded in color.
    - The current status of a device can be determined either of the attributes `ChA` or `ChB` found in full current status of the device.
    - If either of the attributes is not present, then color code the status as Offline.
    - ChA == 2 => On: Yellow
    - ChA == 1 => Antismog: Blue
    - ChA == 0 => Off: Grey
    - Offline/Unknown/Other: Black

Proposed timelimit

- one day

Objective
 
- To guage the understanding of the candidate with respect to the libraries: *React* and *redux*.
- To attain basic familiarity with *IoT.js*
- Optionally, to guage the candidate's familiarity with frontend development through *npm + webpack* workflow.
- Behind the above criteria lies the underlying goal of the ability of the candidate to grasp the architecture of the project.
- The knowledge and application of the tools and technologies (rather than simply outlining the final output) is emphasized because
  - the candidate is expected to integrate with an existing project built with above technologies.
  - an accurate application of mentioned technologies would also testify the candidate's understanding of the underlying architecture of the project in question: state management + functional-influenced paradigm
- Keeping in mind the above criteria as well as the imposed time limit to the completion of the exercise, it is imperative that the application of the mentioned technologies is more important than visual styling of the final output.

Expectations in implementation

- To fetch data used in the exercise, using *IoT.js*.
- To manage the fetched data and app state using *redux*.
- To display the data using *react* components.


Notes on IoT.js

- IoT.js API and getting started guide: http://www.isotel.eu/IoT/doc 