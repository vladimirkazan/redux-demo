import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Devices from '../components/Devices'
import * as DevicesActions from '../actions/devices'

function mapStateToProps(state) {
  return {
    devices: state.devices
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(DevicesActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Devices)
